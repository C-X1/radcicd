"""
Behave steps for testing verilog modules
"""
from pathlib import Path
import re
from behave import given, when, then  # pylint: disable=no-name-in-module
from behave import use_step_matcher

import simulation_verilator

use_step_matcher("re")

def auto_trace_file_name(context):
    if 'trace_file' not in context:
        context.trace_file = Path(context.top_file).with_suffix('')

def multiplier_from_unit(prefix):
    """
    Convert a prefix from a unit as nano, micro, pico...
    into a multiplier
    """
    prefixes = {
        'T': 10**12,
        'G': 10**9,
        'M': 10**6,
        'k': 10**3,
        'd': 10**-1,
        'c': 10**-2,
        'm': 10**-3,
        'µ': 10**-6,
        'u': 10**-6,
        'n': 10**-9,
        'p': 10**-12,
    }
    if prefix in prefixes:
        return prefixes[prefix]

    raise ValueError("Prefix {} not supported".format(prefix))

def convert_unit(string):
    """
    Function to convert a string which contains a number with unit
    """
    result = {}
    result['full_string'] = string
    match = re.match('(?P<number>[0-9]+) *(?P<prefix>.)?(?P<base_unit>.)', string)
    result['number'] = float(match.group('number'))
    if 'unit' in match.groups():
        result['full_unit'] = match.group('unit')
    result['base_unit'] = match.group('base_unit')
    result['multiplier'] = multiplier_from_unit(match.group('prefix'))
    return result

@given(u'the top file "(?P<top_file>.*)"(?P<paths> with the paths)?')
def step_topfile(context, top_file, paths):
    """
    Function to specify the top_module
    """
    context.top_file = top_file
    context.verilog_path = []
    if paths:
        context.verilog_path = [line['Path'] for line in context.table]


@given(u'timebase "(?P<unit>.*)/(?P<precision>.*)"')
def step_timebase(context, unit, precision):
    """
    Function to specify the timebase
    """
    context.timebase = convert_unit(unit)
    context.timebase = convert_unit(precision)

@given(u'a clock starting at "(?P<init>[01])" with "(?P<half_period>[0-9]+.?[0-9]{1,})" time units in each state on "(?P<clk_input>.*)"') # pylint: disable=line-too-long
def step_state_changes(context, init, half_period, clk_input):
    """
    Create a clock on a pin
    """
    if 'clocks' not in context:
        context.clocks = {}

    assert clk_input not in context.clocks, "Error multiple clocks for {}".format(clk_input)

    context.clocks[clk_input] = {
        'init_value': int(init),
        'clk_input': clk_input,
        'half_period': float(half_period)
    }


@when(u'the following state changes are applied to the inputs')
def step_state_changes(context):
    """
    Function to specify the state changes
    """
    context.input_table = context.table

@then(u'the outputs should go through the following states')
def step_result_check(context):
    """
    Function to specify the simulation results
    """
    auto_trace_file_name(context)
    context.output_table = context.table
    simulation_verilator.simulate(context)
