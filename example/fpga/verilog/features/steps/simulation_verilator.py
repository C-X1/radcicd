"""
Verilator
"""

import pyverilator
from verilog_number import VerilogNumber

VERILATOR_BUILDS = {}

def add_to_action_table(what, table, actions):
    """
    Add input and output tables to actions
    """
    current_time = 0
    for row_num, row in enumerate(table):
        rowd = row.as_dict()
        delay = float(rowd[':delay'])
        rowd.pop(':delay')
        current_time += delay

        if current_time not in actions:
            actions[current_time] = {}

        actions[current_time][what] = rowd
        actions[current_time][what + "_row"] = row_num
    return current_time

def add_clocks_to_actions(clocks, max_time, actions):
    """
    Add clock states to actions
    """
    for clock_key in clocks:
        clock = clocks[clock_key]
        init = clock['init_value']
        half_period = clock['half_period']
        clk_input = clock['clk_input']

        if init is None:
            init = False

        current_value = init > 1
        current_time = 0

        for mult in range(int(max_time / half_period + 1)):
            current_time = half_period * mult
            if current_time not in actions:
                actions[current_time] = {}
            if 'input' not in actions[current_time]:
                actions[current_time]['input'] = {}

            assert clk_input not in actions[current_time]['input'], \
                "Error clocked value in input table"

            actions[current_time]['input'][clk_input] = \
                int(current_value)

            current_value = not current_value
            current_time += half_period


def _simulation_action_table(context):
    """
    Add clocks to action table
    """

    input_table = context.input_table
    output_table = context.output_table
    clocks = context.clocks if ('clocks' in context) else {}

    actions = {}

    input_max_time = add_to_action_table('input', input_table, actions)
    output_max_time = add_to_action_table('output', output_table, actions)
    max_time = max(input_max_time, output_max_time)
    add_clocks_to_actions(clocks, max_time, actions)

    return actions

def _build_verilator(context):
    """
    Build verilator model
    """
    if context.top_file not in VERILATOR_BUILDS:
        VERILATOR_BUILDS[context.top_file] = \
            pyverilator.PyVerilator.build(context.top_file,
                                          context.verilog_path,
                                          build_dir='obj_dir',
                                          json_data=(),
                                          gen_only=False,
                                          quiet=True,
                                          command_args=(),
                                          verilog_defines=())
    sim = VERILATOR_BUILDS[context.top_file]
    sim.auto_eval = False
    sim.auto_tracing_mode = None

    context.sim = sim

def _confirm_output_states(sim, cur_action):
    """
    Check the output states if they confirm to the test values
    :sim: Verilator simulation
    :cur_action: current_simulation actions
    """
    outputs = cur_action.get('output', {})
    for output_id in outputs:

        verilogn1 = VerilogNumber.from_string(outputs[output_id])
        verilogn2 = VerilogNumber()
        verilogn2.value = sim.io[output_id]
        verilogn2.vtype = "int"

        assert verilogn1 == verilogn2, f"{verilogn1} is not equal {verilogn2}"

def _evaluate_inputs(sim, cur_action):
    """
    Apply the current states of the simulation
    :sim: Verilator simulation
    :cur_action: current_simulation actions
    """
    inputs = cur_action.get('input', {})
    for input_id in inputs:
        verilog_value = VerilogNumber.from_string(str(inputs[input_id]))
        if verilog_value.vtype != "int":
            raise RuntimeError(f"String does not result to an integer: {inputs[input_id]}")
        sim.io[input_id] = verilog_value.value

def simulate(context):
    """
    Simulation in verilator
    """

    _build_verilator(context)

    sim = context.sim
    context.vcd_dir = 'obj_dir'
    trace_file = f"{context.vcd_dir}/{context.trace_file}.vcd"

    sim.start_vcd_trace(trace_file)

    actions = _simulation_action_table(context)
    for action in sorted(actions.keys()):
        _confirm_output_states(sim, actions[action])
        _evaluate_inputs(sim, actions[action])
        sim.eval()
        sim.add_to_vcd_trace()

    sim.stop_vcd_trace()
