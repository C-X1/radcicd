# language: en
Feature: Simple Wire Module
  Scenario: The FPGA Hello World
     Given the top file "simple_wire.v" with the paths:
       | Path                        |
       | implementations/simple_wire |
     And timebase "1ns/1ns"
     When the following state changes are applied to the inputs:
      | :delay | in |
      |      0 |  0 |
      |      2 |  1 |
      |      4 |  0 |
      |      9 |  1 |
      |     12 |  0 |
    Then the outputs should go through the following states:
      | :delay | out |
      |      1 |   0 |
      |      2 |   1 |
      |      4 |   0 |
      |      9 |   1 |
      |     12 |   0 |
