# language: en
Feature: Simple State Machine 0
  Scenario: A simple state machine
     Given the top file "simple_state_machine0.v" with the paths:
       | Path                                  |
       | implementations/simple_state_machine0 |
     And timebase "1ns/1ns"
     And a clock starting at "0" with "1.0" time units in each state on "clk"
     When the following state changes are applied to the inputs:
       | :delay | in | reset |
       |      0 |  1 |     1 |
       |      2 |  1 |     0 |
       |      2 |  0 |     0 |
       |      2 |  0 |     0 |
       |      5 |  0 |     1 |
    Then the outputs should go through the following states:
      | :delay | out |
      |      3 |   1 |
      |      2 |   1 |
      |      2 |   0 |
      |      2 |   1 |
      |      2 |   0 |
      |      1 |   1 |
