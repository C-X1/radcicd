# language: en
Feature: D-FlipFlop 0
  Scenario: A basic simple D-flipflop
     Given the top file "d_flipflop0.v" with the paths:
       | Path                        |
       | implementations/d_flipflop0 |
     And timebase "1ns/1ns"
     And a clock starting at "0" with "0.5" time units in each state on "clk"
     When the following state changes are applied to the inputs:
       | :delay | d |
       |      0 | 0 |
       |      1 | 1 |
       |      1 | 0 |
       |      1 | 1 |
       |      1 | 0 |
    Then the outputs should go through the following states:
      | :delay | q |
      |      1 | 0 |
      |      1 | 1 |
      |      1 | 0 |
      |      1 | 1 |
      |      1 | 0 |
