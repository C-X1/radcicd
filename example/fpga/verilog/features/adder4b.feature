# language: en
Feature: A 4 Bit Adder
  Scenario: 4 Bit Adder
     Given the top file "adder4b.v" with the paths:
       | Path                    |
       | implementations/adder4b |
       | implementations/base    |
     And timebase "1ns/1ns"
     When the following state changes are applied to the inputs:
       | :delay |     in0 |     in1 |
       |      0 | 4'b0001 | 4'b0010 |
       |      2 |    4'h2 |       2 |
       |      4 |       6 |       4 |
       |      9 |       4 |       4 |
       |     12 |       6 |       7 |
       |     10 |       5 |       5 |
    Then the outputs should go through the following states:
      | :delay | sum     |
      |      1 | 4'b0011 |
      |      2 | 4'h4    |
      |      4 | 4'ha    |
      |      9 | 4'h8    |
      |     12 | 13      |
      |     10 | 4'b10x0 |
