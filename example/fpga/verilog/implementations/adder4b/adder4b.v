/**
 * A 4 Bit adder
 */
module adder4b (input [3:0]  in0,
                input [3:0]  in1,
                output [4:0] sum);
   reg [3:0]                cout;
   base_full_adder add[3:0](.in0(in0[3:0]),
                            .in1(in1[3:0]),
                            .cin({cout[2:0], 1'b0}),
                            .cout(cout[3:0]),
                            .sum(sum[3:0]));
    assign sum[4] = cout[3];
endmodule
