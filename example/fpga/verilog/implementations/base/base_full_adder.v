/*
 * A simple full adder for other designs
 */
module base_full_adder(input in0,
                       input  in1,
                       input  cin,
                       output cout,
                       output sum);

    assign cout = ((~in0 & in1 & cin) |
                   (in0 & ~in1 & cin) |
                   (in0 & in1 & ~cin) |
                   (in0 & in1 & cin));

    assign sum = ((in0 ^ in1 ^ cin) |
                  (in0 & in1 & cin));
endmodule
