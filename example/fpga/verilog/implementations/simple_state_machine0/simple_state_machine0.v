module simple_state_machine0(input  clk,
                             input  reset,
                             input  in,
                             output reg out);

    parameter A=1'b0, B=1'b1;
    reg state, next_state;

    initial
      begin
      out = B;
      end

    always @(*)
        case (state)
          A: next_state = ~in ? B : A;
          B: next_state = ~in ? A : B;
        endcase // case (state)


    always @(posedge clk) begin
        if (reset)
          state <= B;
        else
          state <= next_state;
    end

   assign out = state;
endmodule
