module d_flipflop0 (input      clk,
                    input      d,
                    output reg q );
   initial
     q = 0;
   always @(posedge clk)
     q <= d;
endmodule
