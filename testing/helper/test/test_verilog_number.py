"""
Test VerilogUnitTest
"""
import sys
from os.path import dirname, realpath, join
import unittest
import pytest


class VerilogNumberTest(unittest.TestCase):
    """
    Test VerilogUnitTest
    """
    # pylint: disable=protected-access

    def setUp(self):
        sys.path.append(join(dirname(realpath(__file__)), ".."))
        self.modvn = __import__('verilog_number')
        self.clvn = self.modvn.VerilogNumber()

    def test_reset(self):
        """
        Just a simple execution test
        """
        self.clvn.reset()

    def test_any_char_in_string(self):
        """
        Test for function checking for any char in a string
        """
        assert self.clvn._any_char_in_string("adx", "abc")
        assert self.clvn._any_char_in_string("adc", "abc")
        assert not self.clvn._any_char_in_string("def", "abc")

    def test_convert_hex_octal_to_binary_hex_oct(self):
        """
        Test hex/octal to binary string
        """
        for convf, base in [[hex, 16], [oct, 8]]:
            for input_value in range(0, 2**16):
                input_string = convf(input_value)[2:]
                result_string = self.clvn._convert_hex_octal_to_binary(
                    input_string,
                    base
                )
                result_value = int(result_string, 2)
                self.assertEqual(
                    input_value,
                    result_value,
                    "input_value={input_value}, "
                    "input_string={input_string}, "
                    "result_string={result_string}".format(**locals())
                )

    def test_convert_hex_octal_to_binary_hex_oct_xy(self):
        """
        Test values containing x and z
        """
        for input_string, base, expected_string in [
                ['xzf', 16, 'xxxxzzzz1111'],
                ['axzf', 16, '1010xxxxzzzz1111'],
                ['abcxz', 16, '101010111100xxxxzzzz'],
                ['z', 16, 'zzzz'],
                ['x', 16, 'xxxx'],
                ['x3z7', 8, 'xxx011zzz111'],
                ['z', 8, 'zzz'],
                ['x', 8, 'xxx']
        ]:

            result_string = self.clvn._convert_hex_octal_to_binary(
                input_string,
                base
            )
            self.assertEqual(
                expected_string,
                result_string,
                "input_string={input_string}, "
                "result_string={result_string}".format(**locals())
            )

    def test_convert_int(self):
        """
        Test int conversion #TODO
        """



    def test_extend_binary_to_width(self):
        """
        Test function for extending a binary string
        """
        for input_string, width, signed, expected_string in [
                ["10010", 8, False, "00010010"],
                ["x10011", 8, False, "xxx10011"],
                ["z10100", 8, False, "zzz10100"],
                ["1x", 8, True, "1111111x"],
                ["1", 4, False, "0001"],
                ["0", 3, False, "000"],
                ["z", 3, False, "zzz"],
                ["x", 2, False, "xx"],
                ["001000", 4, False, "1000"]
        ]:
            result_string = self.clvn._extend_binary_to_width(
                input_string, width, signed
            )
            self.assertEqual(
                expected_string,
                result_string,
                "input_string={input_string}, "
                "result_string={result_string}".format(**locals())
            )

    def test_convert_int_float(self):
        """
        Test conversion function for plain int and float
        """
        for input_string, expected_vtype in [
                ["1", int],
                ["3", int],
                ["99", int],
                ["1.3", float],
                ["10.4", float],
                ["203.3", float]
        ]:
            type_name = expected_vtype().__class__.__name__

            self.clvn._convert_int_float(input_string)
            self.assertEqual(self.clvn.vtype, type_name)
            self.assertEqual(self.clvn.value, expected_vtype(input_string))

    def test_x_or_z(self):
        """
        Testing single x and z value
        """
        for input_string in [
                "z", "Z", "x", "X"
        ]:
            self.clvn._x_or_z(input_string)
            self.assertEqual(self.clvn.xzmask, input_string.lower())
            self.assertEqual(self.clvn.vtype, "xz-mask")

    @classmethod
    def none_or_bin(cls, value):
        """
        Return None if none, binary string as int and others directly
        """
        if value is None:
            return None

        if isinstance(value, (int, float)):
            return value

        return int(value, 2)

    def test_from_string(self):
        """
        from String Test
        """
        test_number = 0
        for input_string, value, xzmask, vtype in [
                ["8'bzx1001", None, "zzzx1001", "xz-mask"],
                ["6'bzx1001", None, "zx1001", "xz-mask"],
                ["4'b1001", 9, None, "int"],
                ["-4s'b0001", -1, None, "int"],
                ["-5s'd10", -10, None, "int"],
                ["4", 4, None, "int"],
                ["4'd10", 10, None, "int"],
                ["x", None, "x", "xz-mask"],
                ["z", None, "z", "xz-mask"],
        ]:
            inst = self.modvn.VerilogNumber.from_string(input_string)
            self.assertEqual(inst.xzmask, xzmask, test_number)
            self.assertEqual(inst.value, value, test_number)
            self.assertEqual(inst.vtype, vtype, test_number)
            test_number += 1

    def test_from_string_exception(self):
        """
        Test Exception when not a string
        """
        for input_value in [
                [], 1.2, 1, {}
        ]:
            with pytest.raises(ValueError):
                self.modvn.VerilogNumber.from_string(input_value)

    def test_equality_eq(self):
        """
        Testing equal check
        """
        for index, values in enumerate([
                ["1", "1"],
                ["-1", "-1"],
                ["8'hxx", "8'h1"],
                ["8'hx1", "8'h1"],
                ["8'bxx", "8'h2"],
                ["8'bzz", "8'hzz"],
                ["8'b1xx1", "8'b01111"],
                ["8'b10101xz1", "8'b101011z1"],
                ["8'b10101xz1", "8'b10101zz1"],
                ["8'b10101xz1", "8'b10101zz1"],
                ["6'b101xz1", "8'b00101zz1"],
                ["4'o1", "1"],
                ["4'o10", "8"]
        ]):
            vn1 = self.modvn.VerilogNumber.from_string(values[0])
            vn2 = self.modvn.VerilogNumber.from_string(values[1])
            self.assertEqual(vn1, vn2, "Error in value set {}".format(index))

    def test_equality_neq(self):
        """
        Testing not equal
        """
        for index, values in enumerate([
                ["6'b101xz1", "8'b10101zz1"],
        ]):
            vn1 = self.modvn.VerilogNumber.from_string(values[0])
            vn2 = self.modvn.VerilogNumber.from_string(values[1])
            self.assertNotEqual(vn1, vn2, "Error in value set {}".format(index))

if __name__ == '__main__':
    unittest.main()
