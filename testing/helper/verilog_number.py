"""
VerilogUnit is a class to use different verilog unit notations
including x and z parts for comparison
"""
import re
import numpy as np

class VerilogNumber:
    """
    Class to generate and compare pendants to verilog unit notation
    in testing especially for the binary notation 8'b01010101
    """
    # pylint: disable=too-many-instance-attributes
    def __init__(self):
        self.signed = False
        self.value = None
        self.xzmask = None
        self.vtype = "None"
        self.z_equal_x = False

    def reset(self):
        """
        Reset object to defaults
        """
        self.__init__()

    @classmethod
    def _any_char_in_string(cls, chars, string):
        return True in [c in string for c in chars]

    @classmethod
    def _convert_hex_octal_to_binary(cls, value_string, base):
        """
        Internal function to expand hex or octal to binary
        """
        if not base in [8, 16]:
            return value_string

        conversion_size = 4
        if base == 8:
            conversion_size = 3

        result = ""
        for char in value_string:
            if char in "xz": # Expand
                result += char * conversion_size
            else:
                binary_form = bin(int(char, base))[2:]
                fill_size = 0
                fill_size = conversion_size - len(binary_form)
                result += (fill_size * '0') + (binary_form)

        return result

    def _convert_int(self, value_string, minus, base, width, signed):
        """
        Internal function to convert any forms of integer notation
        """
        if base != 10:
            value_string = self._convert_hex_octal_to_binary(value_string, base)
            value_string = self._extend_binary_to_width(value_string, width, signed)
            base = 2
            if self._any_char_in_string("xz", value_string): # Pattern with don't care / high-imp
                self.xzmask = value_string
                self.vtype = "xz-mask"
                return
        try:
            self.value = int(value_string, base)
            if minus:
                self.value *= -1
            self.vtype = "int"
        except ValueError:
            pass

    @classmethod
    def _extend_binary_to_width(cls, value_string, width, signed):
        """
        Extends binary string to the given width
        According to:
        http://web.engr.oregonstate.edu/~traylor/ece474/beamer_lectures/verilog_number_literals.pdf
        0 and 1 are extended to 0
        x is extended with x
        z is extended with z
        """
        if not width:
            return value_string

        first_char = value_string[0].lower()
        extend_char = "0"

        if (first_char in ["x", "z"]) or (signed and first_char == "1"):
            extend_char = first_char


        width_difference = width - len(value_string)
        if width_difference > 0:
            return (extend_char * width_difference) + value_string

        return value_string[-width_difference:]



    def _convert_verilog_value(self, prefix, value_string, minus, width, signed):
        """
        Function to convert verilog numbers depending on type
        """
        self._convert_int(value_string,
                          minus,
                          {
                              'd': 10,
                              'h': 16,
                              'o': 8,
                              'b': 2,
                          }[prefix],
                          width,
                          signed)
# TODO
#               if minus:
#                    new_high_mask = self.lowmask
#                    self.lowmask = self.highmask
#                    self.highmask = new_high_mask

    def _convert_verilog(self, unit_string):
        match = re.match(
            ("(?P<minus>-)?"
             "(?P<width>[0-9]+)?"
             "(?P<signed>[s])?"
             "'"
             "(?P<prefix>[dbho])"
             "(?P<value>[0-9a-fxz_]+)"),
            unit_string.lower().replace('_', '')
        )

        if match:
            width = int(match.group('width'))
            self.signed = match.group('signed') == "s"
            value = match.group('value').lower()
            prefix = match.group('prefix').lower()
            minus = match.group('minus') == "-"
            self._convert_verilog_value(prefix, value, minus, width, match.group('minus') == 's')

    def _convert_int_float(self, unit_string):
        try:
            self.value = int(unit_string)
            self.vtype = "int"
            return
        except ValueError:
            pass

        try:
            self.value = float(unit_string)
            self.vtype = "float"
        except ValueError:
            pass


    def _x_or_z(self, unit_string):
        unit_string = unit_string.lower()
        if unit_string == "x":
            self.vtype = "xz-mask"
            self.xzmask = unit_string
        elif unit_string == "z":
            self.vtype = "xz-mask"
            self.xzmask = "z"


    def __repr__(self):
        return """
        Value: {}
        Type: {}
        xzmask: {}
        """.format(self.value,
                   self.vtype,
                   self.xzmask)

    @classmethod
    def from_string(cls, unit_string):
        """
        Convert a string to a verilog number class
        """
        if not isinstance(unit_string, str):
            raise ValueError("Not a string!")

        # pylint: disable=protected-access
        obj = VerilogNumber()
        obj.reset()
        obj._x_or_z(unit_string)
        obj._convert_int_float(unit_string)
        obj._convert_verilog(unit_string)
        return obj

    @classmethod
    def _xz_mask_compare(cls, vn0, vn1):
        xzmask0 = vn0.xzmask
        xzmask1 = vn1.xzmask

        lendiff = len(xzmask0) - len(xzmask1)

        if lendiff > 0:
            xzmask1 = cls._extend_binary_to_width(xzmask1,
                                                  len(xzmask1) + lendiff, vn1.signed)

        if lendiff < 0:
            xzmask0 = cls._extend_binary_to_width(xzmask0,
                                                  len(xzmask0) - lendiff, vn0.signed)

        z_equal_x = vn0.z_equal_x or vn1.z_equal_x
        result = True

        for idx in range(len(xzmask0)):
            char_0 = xzmask0[idx]
            char_1 = xzmask1[idx]
            xpresent = (char_0 == 'x' or char_1 == 'x')
            zpresent = (char_0 == 'z' or char_1 == 'z')

            if xpresent or (zpresent and z_equal_x):
                continue

            if char_0 != char_1:
                result = False
                break

        return result

    def _value_to_binary(self):
        if self.vtype == "float":
            raise "Confversion to binary not implemented for float"
        value = self.value

        binary = bin(value).split('b')
        negative = binary[0][0] == '-'
        binary_string = binary[1]

        if negative:
            binary_string = "1" + binary_string

        vnum = VerilogNumber()
        vnum.vtype = "xz-mask"
        vnum.xzmask = binary_string

        return vnum


#Operators
    def __eq__(self, other):

        if self.vtype == "xz-mask" and other.vtype == "xz-mask":
            return self._xz_mask_compare(self, other)


        if self.vtype == "xz-mask" or other.vtype == "xz-mask":
            xzmask = None
            value = None
            if self.vtype == "xz-mask":
                xzmask = self
                value = other._value_to_binary()
            else:
                xzmask = other
                value = self._value_to_binary()

            return self._xz_mask_compare(xzmask, value)

        return self.value == other.value

    def __neq__(self, other):
        return not self.__eq__(other)
