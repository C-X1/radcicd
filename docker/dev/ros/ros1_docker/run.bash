DIR=/home/$USER/.ros1-data

docker run -it \
       --rm \
       --name ROS_1_$(date "+%Y-%m-%d-%H-%M-%S") \
       --gpus all \
       --user=$(id -u $USER):$(id -g $USER) \
       --env="DISPLAY" \
       --volume="/etc/group:/etc/group:ro" \
       --volume="/etc/passwd:/etc/passwd:ro" \
       --volume="/etc/shadow:/etc/shadow:ro" \
       --volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
       --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
       --volume=/tmp/pulse-socket:/tmp/pulse-socket \
       --volume=/dev/snd:/dev/snd \
       --volume=/dev/shm:/dev/shm \
       --device=/dev/snd:/dev/snd \
       --device=/dev/dri:/dev/dri \
       --volume="$DIR:/home/$USER" \
       -w "/home/$USER" \
       ros1_nvidia_glx
